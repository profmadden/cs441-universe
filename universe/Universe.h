//
//  Universe.h
//  universe
//
//  Created by Patrick Madden on 3/1/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Universe : NSObject
@property (nonatomic) int exampleValue;

+(Universe *)sharedInstance;
-(void)saveConfiguration;
-(void)loadConfiguration;
@end
