//
//  AppDelegate.h
//  universe
//
//  Created by Patrick Madden on 3/1/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Universe.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

