//
//  ViewController.m
//  universe
//
//  Created by Patrick Madden on 3/1/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "ViewController.h"
#define LDBG 1

@interface ViewController ()

@end

@implementation ViewController
@synthesize displayValue;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self updateDisplay];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)newValue:(id)sender
{
    [[Universe sharedInstance] setExampleValue:(int)[sender tag]];
    [self updateDisplay];
}

-(void)updateDisplay
{
    [displayValue setText:[NSString stringWithFormat:@"Value is %d", [[Universe sharedInstance] exampleValue]]];
}

////////
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if (LDBG) NSLog(@"Moving to a new view controller");
    if ([segue.identifier isEqualToString:@"toWebView"])
    {
        if (LDBG) NSLog(@"To the web view controller.");
        // segue.destinationViewController is the VC object that we will transition to
    }
}

-(IBAction)done:(UIStoryboardSegue *)segue
{
    if (LDBG) NSLog(@"Roll back to the main view controller");
}
@end
