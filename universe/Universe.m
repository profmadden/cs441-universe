//
//  Universe.m
//  universe
//
//  Created by Patrick Madden on 3/1/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import "Universe.h"
static Universe *singleton = nil;

@implementation Universe
@synthesize exampleValue;

-(id)init
{
    if (singleton)
        return singleton;
    
    self = [super init];
    if (self)
    {
        singleton = self;
    }
    return self;
}

+(Universe *)sharedInstance
{
    return singleton;
}

-(void)loadConfiguration
{
    NSLog(@"Loading configuration.");
    NSArray *dirs = [[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask];
    NSError *err;
    [[NSFileManager defaultManager] createDirectoryAtURL:[dirs objectAtIndex:0] withIntermediateDirectories:YES attributes:nil error:&err];
    NSURL *config = [NSURL URLWithString:@"universe.archive" relativeToURL:[dirs objectAtIndex:0]];
    
    NSData *data = [NSData dataWithContentsOfURL:config];
    if (data)
    {
        NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
        if ([unarchiver containsValueForKey:@"magicNumber"])
        {
            exampleValue = (int)[unarchiver decodeIntForKey:@"magicNumber"];
            NSLog(@"Loaded in magic number %d", exampleValue);
        }
    }
}

-(void)saveConfiguration
{
    NSLog(@"Saving configuration.");
    NSArray *dirs = [[NSFileManager defaultManager] URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask];
    NSError *err;
    [[NSFileManager defaultManager] createDirectoryAtURL:[dirs objectAtIndex:0] withIntermediateDirectories:YES attributes:nil error:&err];
    NSURL *config = [NSURL URLWithString:@"universe.archive" relativeToURL:[dirs objectAtIndex:0]];
    
    
    
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    
    [archiver encodeInt:exampleValue forKey:@"magicNumber"];
    [archiver finishEncoding];
    [data writeToURL:config atomically:YES];
}
@end
