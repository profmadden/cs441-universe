//
//  WebViewController.h
//  universe
//
//  Created by Patrick Madden on 3/1/16.
//  Copyright © 2016 SUNY Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController
@property (nonatomic, strong) IBOutlet UIWebView *webView;
@end
